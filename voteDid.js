
var AGENT_URL_TEST = "https://api-wallet-did-testnet.elastos.org"
var AGENT_URL_PRODUCE = "https://api-wallet-did.elastos.org"

const AGENT_URL = AGENT_URL_PRODUCE

var createDidInfo = (key, value, did) => {
    return {
        "Tag": "DID Property",
        "Ver": "1.0",
        "Status": "Normal",
        "Did": did,
        "Properties": [{
             "Key": key,
             "Value": value,
             "Status": "Normal"
        }]
    }
}

var uploadMemo = (appId, accId, accSecret, key, value, seed, privateKey) => {
    var masterPublicKey = getMasterPublicKey(seed)
    var publicKey = generateSubPublicKey(masterPublicKey, EXTERNAL_CHAIN, 0).toString('hex')
    var did = getDid(publicKey).toString()
    var propKey = "Apps/" + appId + "/" + key

    var didInfo = JSON.stringify(createDidInfo(propKey, value, did))
    console.log("upload didinfo: " + didInfo)
    var memo = {
        "msg": toBinary(didInfo),
        "pub": publicKey,
        "sig": sign(didInfo, privateKey)
    }

    var time = new Date().getTime().toString()
    var hmac = CryptoJS.MD5(time + accSecret)

    var auth = {
        "auth": hmac.toString(),
        "id": accId,
        "time": time
    }

    console.log(JSON.stringify(auth))

    return fetch(AGENT_URL + "/api/1/blockagent/upchain/data", {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-Elastos-Agent-Auth': JSON.stringify(auth)
        },
        body: JSON.stringify(memo)
    })
    .then(response => response.json())
    .then(response => {
        console.log("upload did info response: " + JSON.stringify(response))

        if (response.status === 200) {
            return {
                "txid": response.result,
                "value": value
            }
        }

        return {}
    })
    .catch(function(error) {
        console.log(error);
    })
}

const restoreDid = mnemonic => {
    var seed = getSeedFromMnemonic(mnemonic)
    var masterPublicKey = getMasterPublicKey(seed)
    var publicKey = generateSubPublicKey(masterPublicKey, EXTERNAL_CHAIN, 0).toString('hex')
    var did = getDid(publicKey).toString()

    return {
        "mnemonic": mnemonic,
        "publicKey": publicKey,
        "did": did
    }
}

const createDid = () => {
    var mnemonic = generateMnemonic()
    return restoreDid(mnemonic)
}

const setClaimInfo = (appId, accId, accSecret, key, value, mnemonic) => {
    var seed = getSeedFromMnemonic(mnemonic)
    var privateKey = generateSubPrivateKey(seed, COIN_TYPE_ELA, EXTERNAL_CHAIN, 0).toString('hex')

    return uploadMemo(appId, accId, accSecret, key, value, seed, privateKey)
}

const setSensitiveInfo = (appId, accId, accSecret, key, value, mnemonic) => {
    var seed = getSeedFromMnemonic(mnemonic)
    var privateKey = generateSubPrivateKey(seed, COIN_TYPE_ELA, EXTERNAL_CHAIN, 0).toString('hex')

    return uploadMemo(appId, accId, accSecret, key, sign(value, privateKey), seed, privateKey)
}

const setProofInfo = (appId, accId, accSecret, key, value, thirdDid, mnemonic) => {
    var seed = getSeedFromMnemonic(mnemonic)
    var privateKey = generateSubPrivateKey(seed, COIN_TYPE_ELA, EXTERNAL_CHAIN, 0).toString('hex')

    return uploadMemo(appId, accId, accSecret, key + "/proof/" + thirdDid, value, seed, privateKey)
}

const getInfo = (appId, did, key) => {
    var propKey = "Apps/" + appId + "/" + key
    var url = AGENT_URL + "/api/1/didexplorer/did/" + did + "/property?key=" + encodeURI(propKey)
    return fetch(url, {
        method: 'GET',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(response => {
        console.log("getInfo response: " + JSON.stringify(response))

        if (response.status !== 200) {
            return ""
        }

        var property = JSON.parse(response.result)[0]

        return property.value

    })
    .catch(function(error) {
        console.log(error);
    })
}

const getProofInfo = (appId, did, key, thirdDid) => {
    var proofKey = key + "/proof/" + thirdDid
    return getInfo(appId, did, proofKey)
}

const signData = (message, mnemonic) => {
    var seed = getSeedFromMnemonic(mnemonic)
    var privateKey = generateSubPrivateKey(seed, COIN_TYPE_ELA, EXTERNAL_CHAIN, 0).toString('hex')
    return sign(message, privateKey)
}

const verifyData = (message, signed, publicKey, did) => {
    var genDid = getDid(publicKey).toString()
    if (did ==! genDid) return -1

    return verify(message, signed, publicKey)
}

const verifyClaimInfo = async (appId, did, key, thirdPublicKey) => {
    var srcData = await getInfo(appId, did, key)
    if (srcData === undefined || srcData == "") return false

    var thirdDid = getDid(thirdPublicKey).toString()
    var proofData = await getProofInfo(appId, did, key, thirdDid)
    if (proofData === undefined || proofData == "") return false

    return verifyData(srcData, proofData, thirdPublicKey, did)
}

const verifySensitiveInfo = async (appId, did, key, value, myPublicKey, thirdPublicKey) => {
    var myDid = getDid(myPublicKey).toString()
    if (myDid !== did) return false
    var srcData = await getInfo(appId, did, key)
    if (srcData === undefined || srcData == "") return false

    var ret = verifyData(value, srcData, myPublicKey, did)
    if (!ret) return ret

    var thirdDid = getDid(thirdPublicKey).toString()
    const proofData = await getProofInfo(appId, did, key, thirdDid)
    if (proofData === undefined || proofData == "") return false

    return verifyData(srcData, proofData, thirdPublicKey, did)
}

const thirdPartyLogin = (appName, appId, desc, publicKey, signature) => {
    var did = getDid(publicKey).toString()
    if (did !== appId) return false

    return verifyData(appId, signature, publicKey)
}

const callback = (data, mnemonic, publicKey, callbackUrl) => {
    var json = {
        "Data": data,
        "PublicKey": publicKey,
        "Sign": signData(data, mnemonic)
    }

    return fetch(callbackUrl, {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    })
}

const checkCallback = (data, randomNumber) => {
    var json = JSON.parse(data)
    var ret = verifyData(json.Data, json.Sign, json.PublicKey)
    if (!ret) return ret

    var callbackData = JSON.parse(json.Data)
    return callbackData.RandomNumber === randomNumber
}

